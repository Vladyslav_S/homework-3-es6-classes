"use strict";

/* Обьясните своими словами, как вы понимаете, как работает прототипное
наследование в Javascript

В язык JS везде (кроме простых типов переменных) эсть родитель - прототип. Это такой обьект, который хранит основной функицонал и поведение обьекта */

class Employee {
  constructor({ name, age, salary }) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  set name(name) {
    this._name = name;
  }
  get name() {
    return this._name;
  }
  set age(age) {
    this._age = age;
  }
  get age() {
    return this._age;
  }
  set salary(salary) {
    this._salary = salary;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor({ name, age, salary, lang }) {
    super({ name, age, salary });
    this._lang = lang;
  }
  set salary(salary) {
    this._salary = salary;
  }
  get salary() {
    return this._salary * 3;
  }
}

const progr1 = new Programmer({
  name: "Petya",
  age: "20",
  salary: "1000",
  lang: ["JS", "PHP"],
});
console.log(progr1);

const progr2 = new Programmer({
  name: "Vasya",
  age: "25",
  salary: "2000",
  lang: ["JS", "PHP", "C++"],
});
console.log(progr2);

const progr3 = new Programmer({
  name: "Vasya",
  age: "25",
  salary: "500",
  lang: ["JS"],
});
console.log(progr3);
